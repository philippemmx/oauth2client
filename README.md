# Oauth2client

## Requirement
* [Eclipse](https://www.eclipse.org/)
* [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

I would also recommend using the eclipes [WindowBuilder](https://www.eclipse.org/windowbuilder/) package if you intend on modifying the GUI. This can be done in eclipse by clicking on **Help**, then selecting **Eclipse Marketplace...**, and searching for **WindowBuilder**

Here is the documentation for the eclipse [Standard Widget Tool (SWT)](https://www.eclipse.org/swt/)

## Development
This project is eventually going to be a part of LinGo and intends to integrate with:
1. A local list of minimax accounts defined in an **application.yml**
2. Google accounts
3. Microsoft's Active Directory