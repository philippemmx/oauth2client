package support;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.time.DateUtils;

import model.OAuthToken;

public class TokenStore {

    private Token token;
    private String client;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public synchronized void setToken(OAuthToken tokenResult) {
        if (tokenResult == null) {
            this.token = null;
            return;
        }

        token = new Token();
        token.setAccessToken(tokenResult.getAccessToken());
        token.setExpiresIn(tokenResult.getExpiresIn());
        token.setRefreshToken(tokenResult.getRefreshToken());
        token.setTokenType(tokenResult.getTokenType());
        token.setUserPrincipal(tokenResult.getUserPrincipal());
        token.setExpiresAt(DateUtils.addSeconds(new Date(), token.getExpiresIn().intValue()));

    }

    private Token getToken() {
        if (token == null) {
            return null;
        }

        if (token.getExpiresAt().before(new Date())) {
            token = null;
            return null;
        }

        return token;
    }

    public String getUserPrincipal() {
        Token token = this.getToken();
        if (token == null) {
            return null;
        }
        return token.getUserPrincipal();
    }

    public String getAccessToken() {
        Token token = this.getToken();
        if (token == null) {
            return null;
        }
        return token.getAccessToken();
    }

    private static TokenStore instance;

    private TokenStore() {
    }

    public static TokenStore getInstance() {
        if (null == instance) {
            instance = new TokenStore();
        }
        return instance;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    private static final class Token extends OAuthToken {
        private Date expiresAt;

        public Date getExpiresAt() {
            return expiresAt;
        }

        public void setExpiresAt(Date expiresAt) {
            this.expiresAt = expiresAt;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
