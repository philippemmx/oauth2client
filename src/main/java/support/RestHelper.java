package support;

import okhttp3.OkHttpClient;

public class RestHelper {

    private final OkHttpClient restClient;
    private static RestHelper instance;

    private RestHelper() {
        restClient = new OkHttpClient();
    }

    public static RestHelper getInstance() {
        if (null == instance) {
            instance = new RestHelper();
        }
        return instance;
    }

    public OkHttpClient getRestClient() {
        return restClient;
    }
}
