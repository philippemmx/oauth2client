package support;

public class Constants {
    
    public static final String PROTOCOL = "http";
    
    // Datahub
    public static final String DATAHUB_HOST = "localhost";
    public static final Integer DATAHUB_PORT = 8000;
    public static final String DATAHUB_SUBMIT_AUTHORISATION_CODE_PATH = "code-for-token";
    public static final String DATAHUB_LOGIN_PATH = "login";
    
    // OAuth server
    public static final String OAUTH_SERVER_HOST = "localhost";
    public static final Integer OAUTH_SERVER_PORT = 8081;
    public static final String OAUTH_SERVER_GET_TOKEN_PATH = "auth/oauth/token";

    // Google oauth authorisation code grant type
    public static final String GOOGLE_CLIENT_ID = "414950068714-599gp6hns9v7m5jefm502r2qs1n9141a.apps.googleusercontent.com";
    public static final String GOOGLE_REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
    
    // Local oauth password grant type
    public static final String CLIENT_ID_AND_SECRET = "414950068714-m3bcd14n8mctr05c382bkugbdhvf7nmp.apps.googleusercontent.com:fDw7Mpkk5czHNuSRtmhGmAGL42CaxQB9";
    public static final String OAUTH_CLIENT_ID = "414950068714-m3bcd14n8mctr05c382bkugbdhvf7nmp.apps.googleusercontent.com";
    public static final String OAUTH_CLIENT_SECRET = "fDw7Mpkk5czHNuSRtmhGmAGL42CaxQB9";
    
    // Header type
    public static final String MULTIPART_FORM_HEADER_CONTENT_TYPE = "multipart/form-data";
}
