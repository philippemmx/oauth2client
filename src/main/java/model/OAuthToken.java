package model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class OAuthToken {

    private String accessToken = null;
    private String refreshToken = null;
    private Long expiresIn = null;
    private String tokenType = null;
    private String userPrincipal = null;

    /**
     * access_token
     **/

    @ApiModelProperty(value = "access_token")
    @JsonProperty("access_token")
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * refresh_token
     **/

    @ApiModelProperty(value = "refresh_token")
    @JsonProperty("refresh_token")
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * expires_in
     **/

    @ApiModelProperty(value = "expires_in")
    @JsonProperty("expires_in")
    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * token_type
     **/

    @ApiModelProperty(value = "token_type")
    @JsonProperty("token_type")
    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     * user_principal
     **/

    @ApiModelProperty(value = "user_principal")
    @JsonProperty("user_principal")
    public String getUserPrincipal() {
        return userPrincipal;
    }

    public void setUserPrincipal(String userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OAuthToken oauthToken = (OAuthToken) o;
        return Objects.equals(accessToken, oauthToken.accessToken)
                && Objects.equals(refreshToken, oauthToken.refreshToken)
                && Objects.equals(expiresIn, oauthToken.expiresIn)
                && Objects.equals(tokenType, oauthToken.tokenType)
                && Objects.equals(userPrincipal, oauthToken.userPrincipal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessToken, refreshToken, expiresIn, tokenType, userPrincipal);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class OAuthToken {\n");

        sb.append("    accessToken: ").append(toIndentedString(accessToken)).append("\n");
        sb.append("    refreshToken: ").append(toIndentedString(refreshToken)).append("\n");
        sb.append("    expiresIn: ").append(toIndentedString(expiresIn)).append("\n");
        sb.append("    tokenType: ").append(toIndentedString(tokenType)).append("\n");
        sb.append("    userPrincipal: ").append(toIndentedString(userPrincipal)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
