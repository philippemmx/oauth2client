package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationAdapter;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import okhttp3.OkHttpClient;
import support.RestHelper;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

public class MinimaxLogin extends Shell {

    private static Browser browser;

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            MinimaxLogin shell = new MinimaxLogin(display, "http://localhost:8000/login");
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the shell.
     * 
     * @param display
     */
    public MinimaxLogin(Display display, String url) {
        super(display, SWT.SHELL_TRIM);
        setLayout(new GridLayout(2, false));

        ToolBar toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
        toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

        ToolItem tltmBack = new ToolItem(toolBar, SWT.NONE);
        ToolItem tltmForward = new ToolItem(toolBar, SWT.NONE);
        ToolItem tltmRefresh = new ToolItem(toolBar, SWT.NONE);

        Text txtLocation = new Text(this, SWT.BORDER);
        txtLocation.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.keyCode == SWT.CR) {
                    String modifiedUrl = txtLocation.getText();
                    browser.setUrl(modifiedUrl);
                }
            }
        });
        txtLocation.setText("Location");
        txtLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        browser = new Browser(this, SWT.NONE);

        GridData gd_browser = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
        gd_browser.heightHint = 691;
        browser.setLayoutData(gd_browser);

        /* Add event handling */
        tltmBack.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.back();
            }
        });
        tltmBack.setText("Back");

        tltmForward.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.forward();
            }
        });
        tltmForward.setText("Forward");

        tltmRefresh.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.refresh();
            }
        });
        tltmRefresh.setText("Refresh");

        browser.addLocationListener(new LocationAdapter() {
            @Override
            public void changed(LocationEvent event) {
                if (event.top) {
                    txtLocation.setText(event.location);
                }

                // TODO do something
            }
        });

        browser.setUrl(url);
        createContents();
    }

    /**
     * Create contents of the shell.
     */
    protected void createContents() {
        setText("Minimax Login");
        setSize(640, 780);
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }
}
