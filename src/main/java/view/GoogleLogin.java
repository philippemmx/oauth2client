package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.OAuthToken;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import support.Constants;
import support.RestHelper;
import support.TokenStore;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.browser.LocationAdapter;

import java.io.IOException;
import java.util.function.Consumer;
import org.eclipse.swt.browser.LocationEvent;

public class GoogleLogin extends Shell {

    private static Browser browser;
    private OkHttpClient httpClient = RestHelper.getInstance().getRestClient();

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            GoogleLogin shell = new GoogleLogin(display, "");
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the shell.
     * 
     * @param display
     */
    public GoogleLogin(Display display, String url) {
        super(display, SWT.SHELL_TRIM);
        setLayout(new GridLayout(2, false));

        ToolBar toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
        toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

        ToolItem tltmBack = new ToolItem(toolBar, SWT.NONE);
        ToolItem tltmForward = new ToolItem(toolBar, SWT.NONE);
        ToolItem tltmRefresh = new ToolItem(toolBar, SWT.NONE);

        Text txtLocation = new Text(this, SWT.BORDER);
        txtLocation.setText("Location");
        txtLocation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        browser = new Browser(this, SWT.NONE);

        GridData gd_browser = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
        gd_browser.heightHint = 691;
        browser.setLayoutData(gd_browser);

        /* Add event handling */
        tltmBack.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.back();
            }
        });
        tltmBack.setText("Back");

        tltmForward.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.forward();
            }
        });
        tltmForward.setText("Forward");

        tltmRefresh.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browser.refresh();
            }
        });
        tltmRefresh.setText("Refresh");

        browser.addLocationListener(new LocationAdapter() {
            @Override
            public void changed(LocationEvent event) {
                if (event.top) {
                    txtLocation.setText(event.location);
                }

                if (isGoogleLoginFinished()) {
                    getAccessToken();
                    // do something
                }
            }
        });

        browser.setUrl(url);
        createContents();
    }

    public boolean isGoogleLoginFinished() {
        System.out.println(browser.getUrl());
        return browser.getUrl().contains("https://accounts.google.com/o/oauth2/approval");
    }

    public void getAccessToken() {

        // get OAuth code from URL
        HttpUrl responseUrl = HttpUrl.parse(browser.getUrl());

        // TODO parse this to check for "code" or "error"
        String responseParam = responseUrl.queryParameter("response");
        String googleAuthorisationCode = responseParam.substring(responseParam.indexOf("=") + "=".length());

        System.out.println(googleAuthorisationCode);

        HttpUrl url = new HttpUrl.Builder().scheme(Constants.PROTOCOL)
                .host(Constants.DATAHUB_HOST)
                .port(Constants.DATAHUB_PORT)
                .addPathSegment(Constants.DATAHUB_SUBMIT_AUTHORISATION_CODE_PATH)
                .addQueryParameter("code", googleAuthorisationCode)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response);

            // decode the response body
            ObjectMapper objectMapper = new ObjectMapper();
            String responseBody = response.body().string();

            OAuthToken token = objectMapper.readValue(responseBody, OAuthToken.class);

            // Store token
            TokenStore.getInstance().setToken(token);
            TokenStore.getInstance().setClient("Google");

            // todo: shut down shell
        } catch (IOException e) {
            // TODO show error and shut down window
            e.printStackTrace();
        }
    }

    /**
     * Create contents of the shell.
     */
    protected void createContents() {
        setText("Google Login");
        setSize(640, 780);
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }
}
