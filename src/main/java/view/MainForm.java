package view;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import okhttp3.HttpUrl;
import support.Constants;

import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;

import java.text.MessageFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class MainForm {

    private Shell shellPickLogin;
    private Display display;
    private MinimaxLogin minimaxLogin;
    private GoogleLogin googleLogin;

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String[] args) {
        try {
            MainForm window = new MainForm();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the window.
     */
    public void open() {
        display = Display.getDefault();

        // init authentication choice
        initPickAuthenticationMethod();
        shellPickLogin.open();
        shellPickLogin.layout();

        while (!shellPickLogin.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    protected void initPickAuthenticationMethod() {

        shellPickLogin = new Shell();
        shellPickLogin.setSize(450, 300);
        shellPickLogin.setText("Minimax Login");
        shellPickLogin.setLayout(new FormLayout());

        Button btnContinueWithMinimax = new Button(shellPickLogin, SWT.NONE);
        btnContinueWithMinimax.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {

                HttpUrl url = new HttpUrl.Builder().scheme(Constants.PROTOCOL)
                        .host(Constants.DATAHUB_HOST)
                        .port(Constants.DATAHUB_PORT)
                        .addPathSegment(Constants.DATAHUB_LOGIN_PATH)
                        .build();

                minimaxLogin = new MinimaxLogin(display, url.toString());
                minimaxLogin.open();
                minimaxLogin.getLayout();
            }
        });

        FormData fd_btnContinueWithMinimax = new FormData();
        fd_btnContinueWithMinimax.right = new FormAttachment(100, -127);
        btnContinueWithMinimax.setLayoutData(fd_btnContinueWithMinimax);
        btnContinueWithMinimax.setText("Continue with Minimax");

        Button btnContinueWithGoogle = new Button(shellPickLogin, SWT.NONE);
        fd_btnContinueWithMinimax.bottom = new FormAttachment(btnContinueWithGoogle, -6);
        fd_btnContinueWithMinimax.left = new FormAttachment(btnContinueWithGoogle, 0, SWT.LEFT);
        btnContinueWithGoogle.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {

                String oob = Constants.GOOGLE_REDIRECT_URI;
                String clientId = Constants.GOOGLE_CLIENT_ID;
                String responseType = "code";
                String scope = "email";

                String url = MessageFormat.format(
                        "https://accounts.google.com/o/oauth2/v2/auth?client_id={0}&redirect_uri={1}&response_type={2}&scope={3}",
                        clientId, oob, responseType, scope);

                googleLogin = new GoogleLogin(display, url);
                googleLogin.open();
                googleLogin.getLayout();
            }
        });

        FormData fd_btnContinueWithGoogle = new FormData();
        fd_btnContinueWithGoogle.top = new FormAttachment(0, 128);
        fd_btnContinueWithGoogle.right = new FormAttachment(100, -127);
        fd_btnContinueWithGoogle.left = new FormAttachment(0, 125);
        btnContinueWithGoogle.setLayoutData(fd_btnContinueWithGoogle);
        btnContinueWithGoogle.setText("Continue with Google");
    }
}
